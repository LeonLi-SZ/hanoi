
#include <iostream>
#include <cstdlib>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <cstring>
#include <math.h>
#include <conio.h>

#include "hanoi.h"

using namespace std;

// a dummy commit for test

static inline void lr(void) {cout<<endl;}
static inline void lr(string msg) {cout<<"==== "<<msg<<" ===="<<endl;}
#define lrs() cout<<"\n=============\n\n"

int main (void) {
    cout<<__DATE__<<"; "<<__TIME__<<"\n\n";
    
    int n;
    printf("Hanoi Tower. Input the number of disks to move (< 10): ");
    scanf("%d", &n);
    
    printf("\n%d plates, need %d steps\n", n, (int)(pow(2, n)- 1)); // power(base, index)
    
    hanoi(n, 'a', 'b', 'c');


    return 0;
}

