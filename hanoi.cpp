#include <iostream>
#include <cstdlib>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <cstring>
#include <math.h>
#include <conio.h>


#include "hanoi.h"

using namespace std;

/**
 * @brief move 1 plate from 'from' to 'to'
 * @param none
 * @return none
 * */
static move(char from, char to);

/**
 * @brief move n plate from 'from' via 'via' to 'to'
 * @param none
 * @return none
 * */
void hanoi(unsigned int n, char from, char via, char to) {
	if(n >= 10) {
		printf("\n\nn too big! must < 10\n\n"); // actually, n can be as big as possible, but the steps will be very very big. steps = 2^n - 1
		return;
	}
	
	if(n==1) {
		/*
		if there is only 1 plate, then move it from 'from' to 'to' directly
		*/
		move(from, to);
	}else{
		/*
		if there are more than 1 plate, then split into 3 sub-steps
		1. move top n-1 plates from 'from' via 'to' to 'via'
		2. move the only 1 plate left on the 'from' to 'to'
		3. move those n-1 plates which is already on 'via' via 'from' to 'to'
		*/
		hanoi(n-1, from, to, via);
		move(from, to);
		hanoi(n-1, via, from, to);
	}
}

static move(char from, char to) {
	printf("%c-->%c\n", from, to);
}


